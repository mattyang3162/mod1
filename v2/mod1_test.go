package mod1

import "testing"

func TestFunc1(t *testing.T) {
	t.Log(Func1("calling func1"))
}

func TestFunc2(t *testing.T) {
	t.Log(Func2("calling func2"))
}
